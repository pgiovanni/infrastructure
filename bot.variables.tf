variable "discord_bot_client_id" {
  type        = string
  description = "Discord Client ID"
  sensitive   = true

  validation {
    condition     = can(regex("^\\d{18}$", var.discord_bot_client_id))
    error_message = "Invalid Client ID was specified!"
  }
}

variable "discord_bot_dev_guild" {
  type        = string
  description = "Discord Development Server ID"

  validation {
    condition     = can(regex("^\\d{18}$", var.discord_bot_dev_guild))
    error_message = "Invalid Development Server ID was specified!"
  }
}

variable "discord_bot_token" {
  type        = string
  description = "Discord API Token"
  sensitive   = true

  /*  validation {
    condition     = can(regex("^[A-Za-z\\d]{24}\\.[\\w-]{6}\\.[\\w-]{27}$", var.discord_bot_token))
    error_message = "Invalid Discord API Token was specified!"
  }*/
}