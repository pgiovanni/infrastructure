locals {
  ssl_include = var.environment == "prod" ? "include includes/ssl.conf;" : "listen 80;"
  nginx_env = [
    "ENVIRONMENT=${var.environment}",
    "DOMAIN=${var.domain}",
    "SSL_ENABLED=${local.ssl_include}",
    "GRAFANA_SUBDOMAIN=${var.grafana_subdomain}",
    "GRAFANA_SITE_SCHEME=${var.environment == "prod" ? "https" : "http"}",
    "GRAFANA_HOST=${var.company_prefix}-grafana",
    "GRAFANA_PORT=3000",
    "DISCORD_INVITE_URL=${var.discord_invite_url}",
  ]
}

# Persistence
resource "docker_volume" "certificates" {
  name = "${var.company_prefix}-certificates"
}

# Docker Images
resource "docker_image" "certbot" {
  count = var.environment == "prod" ? 1 : 0
  name  = "${var.company_name}/certbot"
  build {
    path = "docker/certbot/"
    tag  = ["${var.company_name}/certbot:1.0.0"]
    build_arg = {
      "CLOUDFLARE_TOKEN" = var.cloudflare_api_key
    }
    label = {
      "author" = var.infrastructure_maintainer
    }
  }
}

resource "docker_image" "nginx" {
  name = "${var.company_name}/nginx"
  build {
    path = "docker/nginx/"
    tag  = ["${var.company_name}/nginx:1.0.0"]
    label = {
      "author" = var.infrastructure_maintainer
    }
  }

  # To Further increase the creation-delay,
  # we ensure that the containers are created
  # before even creating this nginx image.
  depends_on = [
    docker_container.grafana
  ]
}

# Docker Containers
resource "docker_container" "certbot" {
  count = var.environment == "prod" ? 1 : 0
  image = docker_image.certbot[0].latest
  name  = "${var.company_prefix}-certbot"

  env = [
    "ENVIRONMENT=${var.environment}",
    "DOMAIN=${var.domain}",
    "GRAFANA_SUBDOMAIN=${var.grafana_subdomain}",
    "MAIL=${var.mail}"
  ]

  networks_advanced {
    name = docker_network.internal.name
  }

  volumes {
    volume_name    = docker_volume.certificates.name
    container_path = "/etc/letsencrypt"
  }
}

resource "docker_container" "nginx_dev" {
  count = var.environment == "prod" ? 0 : 1
  image = docker_image.nginx.latest
  name  = "${var.company_prefix}-nginx"
  env   = local.nginx_env

  ports {
    internal = 80
    external = 80
  }

  networks_advanced {
    name = docker_network.internal.name
  }

  mounts {
    type   = "bind"
    source = "${abspath(path.root)}/docker/nginx/logs"
    target = "/etc/nginx/logs"
  }

  restart = "unless-stopped"
}

resource "docker_container" "nginx_prod" {
  count = var.environment == "prod" ? 1 : 0
  image = docker_image.nginx.latest
  name  = "${var.company_prefix}-nginx"
  env   = local.nginx_env

  # Ports
  ports {
    internal = 80
    external = 80
  }

  ports {
    internal = 443
    external = 443
  }

  # Networking
  networks_advanced {
    name = docker_network.internal.name
  }

  # Volumes / Persistance
  volumes {
    volume_name    = docker_volume.certificates.name
    container_path = "/etc/letsencrypt"
  }

  mounts {
    type   = "bind"
    source = "${abspath(path.root)}/docker/nginx/logs"
    target = "/etc/nginx/logs"
  }

  restart = "unless-stopped"
}