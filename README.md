# Programmer's Palace Infrastructure
A dockerized infrastructure containing all you need!  
This will build and deploy various Docker containers.

As this project goes on, I will expand this documentation.

## Installation
### Prerequisites
As a pre-requisite for this repository you need to install
[Terraform](https://terraform.io/downloads) and 
[Docker](https://docs.docker.com/get-docker/). After installing
both you can verify whether they are available by opening
a terminal or command line and typing:
**Windows / Command Line:**
```bat
rem If it prints file-paths, Docker is available.
where docker

rem If it prints file-paths Terraform is available.
where terraform
```
**Windows / PowerShell:**
```powershell
# If it prints a table, Docker is available.
Get-Command docker

# If it prints a table, Terraform is available.
Get-Command terraform
```

**Unix / Shell**
```sh
# If it prints a file-path, Docker is available.
which docker

# If it prints a file-path, Terraform is available.
which terraform
```

### Repository Setup
Now you need to fork the GitLab Vision Infrastructure repository.  

After having done that make sure to clone the repository locally.

### Installation Setup
In order to properly initialize / prepare this project you
need to create a `terraform.tfvars` file, so that all variables
that are later used in the individual containers are set! If 
you are running a linux machine you need to add os = "unix" to the
top of you `terraform.tfvars`.

You can utilize the [Example Variable Definitions](./terraform.tfvars)
and re-configure them to your needs in the `terraform.tfvars` file!


After setting all variables the required variables you can simply
execute following commands to spin up a development environment:
```shell
# (First Time Only) Initialize Terraform
terraform init

# (Optional) Format Terraform Files
terraform fmt

# (Optional) Validate Terraform Files
terraform validate

# (Optional) Check the creation-plan
terraform plan

# Apply the configuration
terraform apply
```

### Useful Scripts
Dependency Visualization:
```sh
# Requires `graphviz` to be installed:
terraform graph | dot -Tsvg > graph.svg
```
Variable Listing:
```sh
for file in *.variables.tf; do 
  cat $file | grep "variable \"" | cut -d'"' -f 2
done
```