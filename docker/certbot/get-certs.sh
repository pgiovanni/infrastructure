#!/bin/bash

# Unset on the 2022-04-20
# I was super dumb and re-generated the certificate
# too often, so we hit the rate-limit for that exact
# combination of domains (root + wildcard-subd.).
# That's why I implemented this hotfix (to be removed
# on the 2022-04-20/in a week) to have a different
# combination of domains to circumvent this ratelimit.
# For the future I have set the certbot to use their
# staging environment, which has increased ratelimits
# if we are NOT on the 'PROD' environment.
HOTFIX=true

args=(
  --dns-cloudflare
  --dns-cloudflare-credentials /run/secrets/cloudflare.ini
  -d ${DOMAIN}
  -m $MAIL
  --non-interactive
  --agree-tos
)

if [ "$HOTFIX" = true ]; then
  args+=(
    -d $GRAFANA_SUBDOMAIN.$DOMAIN
    -d invite.$DOMAIN
    -d board.$DOMAIN
  )
else
  args+=(
    -d *.${DOMAIN}
  )
fi

if [ "$ENVIRONMENT" != "prod" ]; then
  args+=(
    --test-cert
  )
fi

certbot certonly "${args[@]}"
/opt/adjust-perms.sh